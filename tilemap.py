import tetris
from pygame import Rect

class TetrisTilemap(object):
	def __init__(self, w, h):
		self.width = w
		self.height = h
		self.clear() 
		
	def draw(self, screen, offset_x, offset_y):
		for y in xrange(self.height):
			for x in xrange(self.width):
				block = self.blocks[y][x]
				if block != tetris.EMPTY:
					image = tetris.images[block]
					rect = Rect(offset_x + x * 32, offset_y + y * 32, 32, 32)
					screen.blit(image, rect)	
		
	# Add a fallen tetris to the tilemap
	def add_tetris(self, t):
		for block in t.blocks():
			self.blocks[block[1] + t.y][block[0] + t.x] = t.shape
			
	# Get the type of block at the given position
	def get(self, x, y):
		return self.blocks[y][x]
	
	# Clear map
	def clear(self):
		self.blocks = [[tetris.EMPTY for x in xrange(self.width)] for y in xrange(self.height)]
		
	# Check if any rows are full and should be removed
	def check_for_full_rows(self):
		result = []
		for y in xrange(self.height):
			if self.is_row_full(y):
				result.append(y)
		
		return result
	
	# Check if the yth row from the top is full
	def is_row_full(self, y):
		for x in xrange(self.width):
			if self.blocks[y][x] == tetris.EMPTY:
				return False
		
		return True
	
	# Remove the rows at the given indexes and shift rows above them down
	def remove_rows(self, rows_to_remove):
		if len(rows_to_remove) == 0:
			return
			
		new_blocks = [[tetris.EMPTY for x in xrange(self.width)] for y in xrange(self.height)]
		new_y = self.height - 1
		for y in reversed(xrange(self.height)):
			if y in rows_to_remove:
				continue
			else:
				new_blocks[new_y] = self.blocks[y]
				new_y -= 1
				
		self.blocks = new_blocks
		
	
	# Check if a tetris can move in the given direction
	def can_tetris_move(self, t, direction):
		# 2 below
		# 4 left
		# 6 right
		xd = 0
		yd = 0
		if direction == 2:
			yd = +1
		if direction == 4:
			xd = -1
		if direction == 6:
			xd = 1
			
		for block in t.blocks():
			new_pos = [block[0] + t.x + xd, block[1] + t.y + yd]
			# Out of bounds
			if (new_pos[0] < 0 or new_pos[0] >= self.width or new_pos[1] >= self.height):
				return False
			# Collision
			if (self.blocks[new_pos[1]][new_pos[0]] != tetris.EMPTY):
				return False
		
		return True
		
	# Check if a tetris can be rotated in its current position
	def can_tetris_rotate(self, t):
		new_blocks = t.rotated_blocks()

		for block in new_blocks:
			new_pos = [block[0] + t.x, block[1] + t.y]
			# Out of bounds
			if (new_pos[0] < 0 or new_pos[0] >= self.width or new_pos[1] >= self.height):
				return False
			# Collision
			if (self.blocks[new_pos[1]][new_pos[0]] != tetris.EMPTY):
				return False
		
		return True