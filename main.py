import pygame
import os 
from tilemap import TetrisTilemap
import tetris
import random
from tetris import Tetris
from pygame import Color
from pygame import Rect
from ui import UI
import baseai

map = TetrisTilemap(10, 15)
next_piece = 0
t = 0
ais = []
ai = 0
t = Tetris(tetris.random_shape(), 4, 0)
ui = UI()

# Find all python files in the "ai" subdirectory
def find_ais():
	global ais
	lst = os.listdir("ai")
	ais = []
	for d in lst:
		s = d
		if not s.startswith("__") and s.endswith(".py"):
			ais.append("ai." + s[0:-3])
			

# Pick an initialize a random AI
def make_ai():
	global ai
	number = random.randint(0, len(ais)-1)
	ai_module = __import__(ais[number], {}, {}, "AI")
	ai = ai_module.AI()

# Get the next move from the current AI and perform it
# Move the current piece down if able, otherwise game over!
def update():
	global t
	global map
	global next_piece
	global ui
	
	for i in xrange(2):
		next_AI_move = ai.get_move(t, map, 1)
		
		if next_AI_move == baseai.LEFT:
			if map.can_tetris_move(t, 4):
				t.x -= 1
				
		if next_AI_move == baseai.RIGHT:
			if map.can_tetris_move(t, 6):
				t.x += 1
		
		if next_AI_move == baseai.ROTATE:
			if map.can_tetris_rotate(t):
				t.rotate()
			
	if (map.can_tetris_move(t, 2)):
		t.y += 1
		ui.increaseCurrentScore(1)
	else:
		map.add_tetris(t)
		rows = map.check_for_full_rows()
		ui.increaseCurrentScore(100 * len(rows))
		map.remove_rows(rows)
		
		t = next_piece
		t.x = int((map.width - 1) / 2)
		t.y = -1
		next_piece = Tetris(tetris.random_shape(), 0, 0)
		if map.can_tetris_move(t, 2):
			t.y += 1
		else:
			draw()
			pygame.time.delay(800)
			reset()

# Clear the stage and select a new AI			
def reset():
	global map
	global t
	global next_piece

	if next_piece == 0:
		next_piece = Tetris(tetris.random_shape(), 0, 0)
	else:
		ui.clearCurrentScore(ai.get_name())
		
	t = next_piece
	t.x = int((map.width - 1) / 2)
	t.y = -1
	next_piece = Tetris(tetris.random_shape(), 0, 0)
	make_ai()
	map.clear()
	
# Draw the stage and UI
def draw():
	screen.fill(Color(0,0,0))
	pygame.draw.rect(screen, 0x333333, Rect(16,0,320,480), 0)
	pygame.draw.rect(screen, 0x333333, Rect(320 + 32 + 70,112,128,64), 0)
	next_piece.draw(screen, 320 + 32 + 32 + 70, 112)
	map.draw(screen, 16, 0)
	t.draw(screen, 16, 0)
	ui.drawTitle(screen, 380, 0)
	ui.drawCurrentScore(screen, ai.get_name(), 360, 66)
	ui.drawNext(screen, 365, 132)
	ui.drawHighScoreList(screen, 360, 196)
	pygame.display.flip()

def quitting():
	for event in pygame.event.get():
			if event.type == pygame.QUIT:
				return True
			elif event.type == pygame.KEYDOWN:
				return True
	
	return False
	
# Main loop
os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
try:
	pygame.display.set_caption("Tretris")
	pygame.mouse.set_visible(False)
	screen = pygame.display.set_mode((640,480),pygame.FULLSCREEN)
	find_ais()
	reset()
	while True:
		if quitting():
			break
		pygame.event.clear()
		draw()
		update()
		pygame.time.delay(50)
		
finally:
	pygame.quit()  # Keep this IDLE friendly 
	
