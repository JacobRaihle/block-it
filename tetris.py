import random
import pygame
from pygame import Rect

EMPTY = 0
I = 1
L = 2
J = 3
Z = 4
S = 5
O = 6
T = 7

images = {
	I: pygame.image.load("assets/blue.png"),
	L: pygame.image.load("assets/yellow.png"),
	J: pygame.image.load("assets/orange.png"),
	Z: pygame.image.load("assets/green.png"),
	S: pygame.image.load("assets/teal.png"),
	O: pygame.image.load("assets/red.png"),
	T: pygame.image.load("assets/purple.png")
}

shapes = {
	I: [[[-1,0],[0,0],[1,0],[2,0]], [[0,0],[0,1],[0,2],[0,3]]],
	L: [[[-1,1],[0,1],[1,1],[1,0]],
		[[0,2],[0,1],[0,0],[-1,0]], [[1,1],[0,1],[-1,1],[-1,2]],[[0,0],[0,1],[0,2],[1,2]]],
	J: [[[-1,0],[0,0],[1,0],[1,1]],
		[[1,2],[1,1],[1,0],[2,0]], [[2,1],[1,1],[0,1],[0,0]],[[1,0],[1,1],[1,2],[0,2]]],
	
	Z: [[[-1,0],[0,0],[0,1],[1,1]], [[1,0],[1,1],[0,1],[0,2]]],
	S: [[[-1,1],[0,1],[0,0],[1,0]], [[0,0],[0,1],[1,1],[1,2]]],
	O: [[[0,0],[0,1],[1,0],[1,1]]],
	T: [[[-1,0],[0,0],[1,0],[0,1]], [[-1,0],[0,-1],[0,0],[0,1]], 
	    [[-1,0],[0,0],[1,0],[0,-1]], [[1,0],[0,-1],[0,0],[0,1]]]
}

def random_shape():
	return random.randint(I, T)

class Tetris(object):
	def __init__(self, shape, x, y):
		self.shape = shape
		self.rotation = 0
		self.x = x
		self.y = y
		
	# The number of rotations available to this piece
	def rotations(self):
		return len(shapes[self.shape])
		
	def blocks(self):
		return shapes[self.shape][self.rotation]
		
	def draw(self, screen, offset_x, offset_y):
		for block in self.blocks():
			rect = Rect(offset_x + 32 * (self.x + block[0]), offset_y + 32 * (self.y + block[1]), 32, 32)
			screen.blit(images[self.shape], rect)

	# Select the next rotation
	def rotate(self):
		self.rotation += 1
		if self.rotation >= self.rotations():
			self.rotation = 0
	
	# Get the offset (from the piece position) of the lowest block
	def lowest_block(self):
		lowest = 0
		for block in self.blocks():
			if block[1] > lowest:
				lowest = block[1]
		
		return lowest
	
	# return the new block orientation after rotation
	# (does not rotate the piece)
	def rotated_blocks(self):
		next_rotation = self.rotation + 1
		if next_rotation >= self.rotations():
			next_rotation = 0
		
		return shapes[self.shape][next_rotation]