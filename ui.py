import pygame

class UI(object):
	
	def __init__(self):
		self.current_score = 0
		self.high_score = 0
		self.high_score_list = []
		pygame.init()
		self.titlefont = pygame.font.SysFont("monospace", 50)
		self.currentscorefont = pygame.font.SysFont("monospace", 22)
		self.nextfont = pygame.font.SysFont("monospace", 16)
		self.highscorefont = pygame.font.SysFont("monospace", 18)
		
	def clearCurrentScore(self, ai_name):
		self.addToHighScoreList(ai_name, self.current_score)
		self.current_score = 0
		
	def addToHighScoreList(self, ai_name, score):
		self.high_score_list.append((ai_name, score))
		self.high_score_list.sort(key=lambda tup: tup[1])
		self.high_score_list = list(reversed(self.high_score_list))
		if len(self.high_score_list) > 10:
			self.high_score_list.pop()
	
	def getIntToFixedString(self, input_int, string_length):
		diff = string_length - len(str(input_int))
		result = ""
		for _ in xrange(diff):
			result += " "
		return result + str(input_int)
	
	def drawTitle(self, screen, x_offset, y_offset):
		labeltitle = self.titlefont.render("TreTris", 1, (255,127,0))
		screen.blit(labeltitle, (x_offset, y_offset))
		
	def drawCurrentScore(self, screen, ai_name, x_offset, y_offset):
		labelcurrent = self.currentscorefont.render(ai_name + ": " + self.getIntToFixedString(self.current_score, 9), 1, (255,255,0))
		screen.blit(labelcurrent, (x_offset, y_offset))
		
	def drawNext(self, screen, x_offset, y_offset):
		labelnext = self.nextfont.render("Next:", 1, (127,127,127))
		screen.blit(labelnext, (x_offset, y_offset))
		
	def drawHighScoreList(self, screen, x_offset, y_offset):
		position = 1
		for i in self.high_score_list:
			line = str(position) + ") " + i[0] + ": "
			line += self.getIntToFixedString(i[1], 23-len(line))
			labelhigh = self.highscorefont.render(line, 1, (255,255,255))
			screen.blit(labelhigh, (x_offset, y_offset + position * 20))
			position += 1
		
	def increaseCurrentScore(self, amount):
		self.current_score += amount
		if self.high_score < self.current_score:
			self.high_score = self.current_score
			
	