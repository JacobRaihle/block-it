import baseai
import random
import tetris

class AI(object):

	@staticmethod
	def get_name():
		return "Greedy AI"

	def __init__(self):
		self.last_tetris = 0
	
	def get_move(self, t, map, moves_until_drop):
		if (t != self.last_tetris):
			self.last_tetris = t
			(self.target_block, self.target_rotation) = self.get_target_block_and_rotation(t, map)
		if (t.rotation == self.target_rotation):
			if t.x < self.target_block:
				return baseai.RIGHT
			elif t.x > self.target_block:
				return baseai.LEFT
			else:
				return baseai.DROP
		else:
			return baseai.ROTATE
			
	
	
	def get_target_block_and_rotation(self, t, m):
		options = []
		old_x = t.x
		highest = 0
		highest_r = 0
		highest_x = 0
		for r in xrange(t.rotations()):
			for x in xrange(m.width):
				t.x = x
				old_y = t.y
				while m.can_tetris_move(t, 2):
					t.y += 1
				
				score = t.y + t.lowest_block()
				t.y = old_y
				if score > highest:
					highest = score
					highest_r = r
					highest_x = x
			t.rotate()
			
		t.x = old_x
		return (highest_x, highest_r)
