import baseai
import random
import tetris

class AI(object):

	@staticmethod
	def get_name():
		return "Lowest AI"

	def __init__(self):
		self.last_tetris = 0
	
	def get_move(self, t, map, moves_until_drop):
		if (t != self.last_tetris):
			self.last_tetris = t
			self.target_block = self.get_target_block(t, map)
			self.target_rotation = self.get_target_rotation(t, map)
			
		if (t.x == self.target_block):
			if (t.rotation == self.target_rotation):
				return baseai.DROP
			else:
				return baseai.ROTATE
		else:
			if t.x < self.target_block:
				return baseai.RIGHT
			else:
				return baseai.LEFT
	
	def get_target_block(self, t, m):
		y = m.height - 1
		cols = range(m.width)
		for y in xrange(m.height):
			next_cols = []
			for x in cols:
				if m.get(x, y) == tetris.EMPTY:
					next_cols.append(x)
					
			if len(next_cols) == 0:
				return cols[0]
			if len(next_cols) == 1:
				return next_cols[0]
			cols = next_cols
		return cols[0]
		
	def get_target_rotation(self, t, m):
		return random.randint(0,t.rotations()-1)
