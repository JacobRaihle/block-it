import baseai
import random


class AI(object):

	@staticmethod
	def get_name():
		return "Random AI"

	def __init__(self):
		self.last_tetris = 0
	
	def get_move(self, t, map, moves_until_drop):
		if (t != self.last_tetris):
			self.last_tetris = t
			self.target_block = self.get_target_block(t, map)
			self.target_rotation = self.get_target_rotation(t, map)
			
		if (t.x == self.target_block):
			if (t.rotation == self.target_rotation):
				return baseai.DROP
			else:
				return baseai.ROTATE
		else:
			if t.x < self.target_block:
				return baseai.RIGHT
			else:
				return baseai.LEFT
	
	def get_target_block(self, t, m):
		return random.randint(-1,m.width)
		
	def get_target_rotation(self, t, m):
		return random.randint(0,t.rotations()-1)
