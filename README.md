# README #

### Quick start ###
* Install python 2.7.8 and pygame 1.9.1
* Run main.py

### About TreTris ###
TreTris is a self-playing Tetris clone. At the start of each game it randomly selects an AI to use. You can add AIs by dropping .py files in the 'ai' directory - see existing files for guidance.